import datetime

from django.contrib.auth.models import User
from django.urls import reverse
from django.utils import timezone
from django.test import TestCase

from .models import Question


def create_question(question_text, days=0):
    """
    Create a question with the given `question_text` and published the
    given number of `days` offset to now (negative for questions published
    in the past, positive for questions that have yet to be published).
    """
    time = timezone.now() + datetime.timedelta(days=days)
    return Question.objects.create(question_text=question_text, pub_date=time)


class QuestionModelTests(TestCase):

    def test_was_published_recently_with_future_question(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is in the future.
        """
        time = timezone.now() + datetime.timedelta(days=30)
        future_question = Question(pub_date=time)
        self.assertIs(future_question.was_published_recently(), False)

    def test_was_published_recently_with_old_question(self):
        """
        was_published_recently() returns False for questions whose pub_date
        is older than 1 day.
        """
        time = timezone.now() - datetime.timedelta(days=1, seconds=1)
        old_question = Question(pub_date=time)
        self.assertIs(old_question.was_published_recently(), False)

    def test_was_published_recently_with_recent_question(self):
        """
        was_published_recently() returns True for questions whose pub_date
        is within the last day.
        """
        time = timezone.now() - datetime.timedelta(hours=23, minutes=59, seconds=59)
        recent_question = Question(pub_date=time)
        self.assertIs(recent_question.was_published_recently(), True)

    def test_get_selected_choice_for_user(self):
        """
        get_selected_choice_for_user() returns selected by user choice for this question
        or None if it does not exist.
        """
        question1 = create_question('question1')
        choice1 = question1.choice_set.create(choice_text='choice1')

        question2 = create_question('question2')
        question2.choice_set.create(choice_text='choice2')

        user = User.objects.create_user('test_user')
        choice1.voted_users.add(user)

        self.assertEqual(question1.get_selected_choice_for_user(user.pk), choice1)
        self.assertEqual(question2.get_selected_choice_for_user(user.pk), None)


class QuestionIndexViewTests(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')
        cls.user_id = user.pk

    def setUp(self):
        super().setUp()
        self.client.post('/accounts/login/', {'username': 'temporary', 'password': 'temporary'})

    def test_no_questions(self):
        """
        If no questions exist, an appropriate message is displayed.
        """
        response = self.client.get(reverse('quiz:index'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, "No not voted questions are available")
        self.assertQuerysetEqual(response.context['not_voted_questions'], [])

    def test_past_question(self):
        """
        Questions with a pub_date in the past are displayed on the
        index page.
        """
        question = create_question(question_text="Past question.", days=-30)
        response = self.client.get(reverse('quiz:index'))
        self.assertQuerysetEqual(
            response.context['not_voted_questions'],
            [question.__repr__()]
        )

    def test_future_question(self):
        """
        Questions with a pub_date in the future aren't displayed on
        the index page.
        """
        create_question(question_text="Future question.", days=30)
        response = self.client.get(reverse('quiz:index'))
        self.assertContains(response, "No not voted questions are available")
        self.assertQuerysetEqual(response.context['not_voted_questions'], [])

    def test_future_question_and_past_question(self):
        """
        Even if both past and future questions exist, only past questions
        are displayed.
        """
        past_question = create_question(question_text="Past question.", days=-30)
        create_question(question_text="Future question.", days=30)
        response = self.client.get(reverse('quiz:index'))
        self.assertQuerysetEqual(
            response.context['not_voted_questions'],
            [past_question.__repr__()]
        )

    def test_two_past_questions(self):
        """
        The questions index page may display multiple questions.
        """
        past_question1 = create_question(question_text="Past question 1.", days=-30)
        past_question2 = create_question(question_text="Past question 2.", days=-5)
        response = self.client.get(reverse('quiz:index'))
        self.assertQuerysetEqual(
            response.context['not_voted_questions'],
            [past_question2.__repr__(), past_question1.__repr__()]
        )

    def test_only_not_voted_and_only_voted(self):
        """
        If only_not_voted param set there are only not voted questions for current user will be in response
        """
        question1 = create_question('question1')
        question2 = create_question('question2')
        choice = question2.choice_set.create(choice_text='choice')
        choice.voted_users.add(self.user_id)
        response = self.client.get(reverse('quiz:index'), data={'only_not_voted': True})
        self.assertQuerysetEqual(
            response.context['not_voted_questions'],
            [question1.__repr__()]
        )
        self.assertEqual(
            response.context['voted_questions'],
            None
        )

        response = self.client.get(reverse('quiz:index'), data={'only_voted': True})
        self.assertEqual(
            response.context['not_voted_questions'],
            None
        )
        self.assertQuerysetEqual(
            response.context['voted_questions'],
            [question2.__repr__()]
        )

    def test_offset_limit(self):
        """
        The offset and limit params are used for paging.
        """
        question1 = create_question(question_text="question1")
        question2 = create_question(question_text="question2")
        question3 = create_question(question_text="question3")
        response = self.client.get(reverse('quiz:index'), data={'offset': 1})
        self.assertQuerysetEqual(
            response.context['not_voted_questions'],
            [question2.__repr__(), question1.__repr__()]
        )

        response = self.client.get(reverse('quiz:index'), data={'limit': 1})
        self.assertQuerysetEqual(
            response.context['not_voted_questions'],
            [question3.__repr__()]
        )

        response = self.client.get(reverse('quiz:index'), data={'limit': 1, 'offset': 1})
        self.assertQuerysetEqual(
            response.context['not_voted_questions'],
            [question2.__repr__()]
        )


class QuestionDetailViewTests(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')
        cls.user_id = user.pk

    def setUp(self):
        super().setUp()
        self.client.post('/accounts/login/', {'username': 'temporary', 'password': 'temporary'})

    def test_future_question(self):
        """
        The detail view of a question with a pub_date in the future
        returns a 404 not found.
        """
        future_question = create_question(question_text='Future question.', days=5)
        url = reverse('quiz:detail', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        """
        The detail view of a question with a pub_date in the past
        displays the question's text.
        """
        past_question = create_question(question_text='Past Question.', days=-5)
        url = reverse('quiz:detail', args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)

    def test_voted_question(self):
        """
        The detail view of a question that has been voted by current user displays
        result of this question.
        """
        question2 = create_question('question2')
        choice = question2.choice_set.create(choice_text='choice')
        choice.voted_users.add(self.user_id)
        url = reverse('quiz:detail', args=(question2.id,))
        response = self.client.get(url)
        self.assertContains(response, 'Your vote')


class QuestionResultsViewTests(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')

    def setUp(self):
        super().setUp()
        self.client.post('/accounts/login/', {'username': 'temporary', 'password': 'temporary'})

    def test_future_question(self):
        """
        The detail view of a question with a pub_date in the future
        returns a 404 not found.
        """
        future_question = create_question(question_text='Future question.', days=5)
        url = reverse('quiz:results', args=(future_question.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, 404)

    def test_past_question(self):
        """
        The detail view of a question with a pub_date in the past
        displays the question's text.
        """
        past_question = create_question(question_text='Past Question.', days=-5)
        url = reverse('quiz:results', args=(past_question.id,))
        response = self.client.get(url)
        self.assertContains(response, past_question.question_text)


class QuestionVoteViewTests(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        user = User.objects.create_user('temporary', 'temporary@gmail.com', 'temporary')
        cls.user_id = user.pk

    def setUp(self):
        super().setUp()
        self.client.post('/accounts/login/', {'username': 'temporary', 'password': 'temporary'})

    def test_vote_future_question(self):
        """
        The detail view of a question with a pub_date in the future
        returns a 404 not found.
        """
        future_question = create_question(question_text='Future question.', days=5)
        response = self.client.post(reverse('quiz:vote', args=(future_question.id,)))
        self.assertEqual(response.status_code, 404)

    def test_vote_past_question(self):
        """
        The detail view of a question with a pub_date in the past
        displays the question's text.
        """
        past_question = create_question(question_text='Past Question.', days=-5)
        choice = past_question.choice_set.create(choice_text='choice')
        response = self.client.post(reverse('quiz:vote', args=(past_question.id,)),
                                    data={'choice_id': choice.pk},
                                    follow=True)
        choice.refresh_from_db()
        self.assertEqual(choice.votes, 1)
        self.assertContains(response, past_question.question_text)
        self.assertContains(response, 'Your vote')

    def test_vote_twice(self):
        """
        The detail view of a question with a pub_date in the past
        displays the question's text.
        """
        past_question = create_question(question_text='Past Question.', days=-5)
        choice = past_question.choice_set.create(choice_text='choice')
        self.client.post(reverse('quiz:vote', args=(past_question.id,)),
                         data={'choice_id': choice.pk},
                         follow=True)
        response = self.client.post(reverse('quiz:vote', args=(past_question.id,)),
                                    data={'choice_id': choice.pk})
        self.assertContains(response, 'You can not vote twice')

    def test_vote_without_choice(self):
        """
        The detail view of a question with a pub_date in the past
        displays the question's text.
        """
        past_question = create_question(question_text='Past Question.', days=-5)
        choice = past_question.choice_set.create(choice_text='choice')
        response = self.client.post(reverse('quiz:vote', args=(past_question.id,)),
                                    follow=True)

        self.assertContains(response, 'You did not choose anything')
