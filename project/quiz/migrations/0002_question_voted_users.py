# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-21 15:17
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('quiz', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='question',
            name='voted_users',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
    ]
