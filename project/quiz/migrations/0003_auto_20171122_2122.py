# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2017-11-22 18:22
from __future__ import unicode_literals

from django.conf import settings
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('quiz', '0002_question_voted_users'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='question',
            name='voted_users',
        ),
        migrations.AddField(
            model_name='choice',
            name='voted_users',
            field=models.ManyToManyField(to=settings.AUTH_USER_MODEL),
        ),
    ]
