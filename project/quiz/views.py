from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.urls import reverse
from django.utils import timezone
from django.views import generic

from .models import Question, Choice


@login_required()
def index(request):
    only_not_voted = request.GET.get('only_not_voted')
    only_voted = request.GET.get('only_voted')
    offset = int(request.GET.get('offset', 0))
    limit = int(request.GET.get('limit', 10))
    not_voted_questions = None
    voted_questions = None
    now = timezone.now()

    if not only_not_voted:
        voted_questions = Question.objects.filter(
            pub_date__lte=now,
            choice__voted_users__pk=request.user.pk).order_by('-pub_date')[offset:offset+limit]
    if not only_voted:
        not_voted_questions = Question.objects.filter(pub_date__lte=now).exclude(
            choice__voted_users__pk=request.user.pk).order_by('-pub_date')[offset:offset+limit]

    context = {
        'not_voted_questions': not_voted_questions,
        'voted_questions': voted_questions,
        'only_not_voted': only_not_voted,
        'only_voted': only_voted,
        'offset': offset,
        'limit': limit
    }
    return render(request, 'quiz/index.html', context)


@login_required()
def detail(request, question_id):
    question = get_object_or_404(Question, pub_date__lte=timezone.now(), pk=question_id)
    selected_choice = question.get_selected_choice_for_user(request.user.pk)
    if selected_choice:
        return render(request, 'quiz/results.html', {'question': question,
                                                     'selected_choice_pk': selected_choice.pk})
    return render(request, 'quiz/detail.html', {'question': question})


@login_required()
def results(request, question_id):
    question = get_object_or_404(Question, pub_date__lte=timezone.now(), pk=question_id)
    context = {'question': question}
    selected_choice = question.get_selected_choice_for_user(request.user.pk)
    if selected_choice:
        context.update({'selected_choice_pk': selected_choice.pk})
    return render(request, 'quiz/results.html', context)


@login_required()
def vote(request, question_id):
    question = get_object_or_404(Question, pub_date__lte=timezone.now(), pk=question_id)
    if question.get_selected_choice_for_user(request.user.pk):
        return render(request, 'quiz/detail.html', {
            'question': question,
            'error_message': 'You can not vote twice'
        })
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice_id'])
    except (KeyError, Choice.DoesNotExist):
        return render(request, 'quiz/detail.html', {
            'question': question,
            'error_message': 'You did not choose anything'
        })
    else:
        selected_choice.votes += 1
        selected_choice.voted_users.add(request.user)
        selected_choice.save()
        return HttpResponseRedirect(reverse('quiz:results', args=(question.id,)))


# not in use
class IndexView(LoginRequiredMixin, generic.ListView):
    template_name = 'quiz/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.filter(pub_date__lte=timezone.now()).order_by('-pub_date')[:5]


# not in use
class DetailView(LoginRequiredMixin, generic.DetailView):
    model = Question
    template_name = 'quiz/detail.html'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())


# not in use
class ResultsView(LoginRequiredMixin, generic.DetailView):
    model = Question
    template_name = 'quiz/results.html'

    def get_queryset(self):
        """
        Excludes any questions that aren't published yet.
        """
        return Question.objects.filter(pub_date__lte=timezone.now())
